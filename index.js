const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

app.use('/src/scripts.js', express.static('./src/scripts.js'));
app.use('/data/weather.json', express.static('./data/weather.json'));

app.get('/weather',(req,res,next) => {
    res.sendFile(path.join(__dirname,'public','index.html'));
});

app.get('/weather/:city',(req,res,next) => {
  res.sendFile(path.join(__dirname,'public',`city-weather.html`));
});

app.post('/weather', (req, res) => {
  res.redirect(`/weather/${req.body.city.toLowerCase()}`);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});